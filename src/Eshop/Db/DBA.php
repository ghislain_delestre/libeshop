<?php
namespace Eshop\Db;
use PDO;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class DBA {
    private $pdo;
    
    public function __construct() {
        $this->pdo = new PDO(
                'mysql:host=localhost;dbname=eshop', 'root', 'root', array (PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    }
    
    public function getPDO(){
        return $this->pdo;
    }
}