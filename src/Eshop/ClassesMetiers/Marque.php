<?php
namespace Eshop\ClassesMetiers;
use Eshop\Db\DBA;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Marque
 *
 * @author sio
 */
class Marque {
    private $idMarque;
    private $libelle;
    private $collectionProduit;
    
    
    public function getIdMarque() {
        return $this->idMarque;
    }

    public function getLibelle() {
        return $this->libelle;
    }
    
    public function setLibelle($libelle){
        $this->libelle=$libelle;
    }
    
    private static function arrayToMarque(Array $array){
        $m = new Marque();
        $m->idMarque = $array["idMarque"];
        $m->libelle = $array["libelle"];
        $m->collectionProduit = Produit::fetchAllByMarque($m);
        return $m;
    }
    
    private static $select = "select * from marque";
    private static $selectById = "select * from marque where idMarque =:idMarque";
    private static $insert = "insert into marque (libelle) values (:libelle)";
    private static $update = "update marque set libelle=:libelle where idMarque=:idMarque";
    private static $delete = "delete from marque where idMarque =:idMarque";
    
     public static function fetchAll(){
        $marques = array();
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->query(Marque::$select);
        $resultat = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
        if(is_array($resultat)){
            foreach ($resultat as $value){
                $marques[] = Marque::arrayToMarque($value);
            }
        }
        return $marques;
    }
    
    public static function fetch($idMarque){
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Marque::$selectById);
        $pdoStatement->bindParam(":idMarque", $idMarque);
        $pdoStatement->execute();
        $record = $pdoStatement->fetch(PDO::FETCH_ASSOC);
        $marque = Marque::arrayToMarque($record);
        return $marque;
    }
    
    public function insert(){
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Marque::$insert);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->execute();
        $this->idMarque = $pdo->lastInsertId();
    }
    
    public function update(){
        $pdo = (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Marque::$update);
        $pdoStatement->bindParam(":idMarque", $this->idMarque);
        $pdoStatement->bindParam(":libelle", $this->libelle);
        $pdoStatement->execute();
    }
    
    public function delete(){
        $pdo= (new DBA())->getPDO();
        $pdoStatement = $pdo->prepare(Marque::$delete);
        $pdoStatement->bindParam(":idMarque", $this->idMarque);
        $resultat = $pdoStatement->execute();
        $nblignesAffectees = $pdoStatement->rowCount();
        if($nblignesAffectees==1){
            $this->idMarque = null;
        }
        return $resultat;
    }
    
    public function save(){
        if($this->idMarque == null){
            $this->insert();
        }else{
            $this->update();
        }
        $this->saveProduits();
    }
    

    
    private function existProduit(Produit $produit){
        $existe = false;
        foreach ($this->collectionProduit as $produitCourant){
            if($produit->compareTo($produitCourant)){
                $existe = true;
                break;
            }
        }
        return $existe;
    }
    
    private function saveProduits(){
        foreach ($this->collectionProduit as $produit){
            $produit->save();
        }
    }
    
    
    public function removeProduit(Produit $produit){
        $new = array();
        foreach ($this->collectionProduit as $produitCourant){
            if(!$produitCourant->compareTo($produit)){
                $new[]=$produitCourant;
                break;
            }
        }
        $this->collectionProduit = $new;
        $produit->setMarque(null);
    }
    
    public function addProduit(Produit $produit){
        if(!$this->existProduit($produit)){
            $this->collectionProduit[] = $produit;
            if(!$produit->getMarque()->compareTo($this)){
                $produit->setMarque($this);
            }
        }
    }
    
    public function compareTo(Marque $marque){
        return $this->idMarque == $marque->idMarque;
    }
    
}
